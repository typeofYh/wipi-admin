import { defineConfig } from 'umi';
import routes from './router';
const MonacoWebpackPlugin = require('monaco-editor-webpack-plugin');
const extraBabelPlugins: string[] = [];
if (process.env.NODE_ENV === 'prodcution') {
  extraBabelPlugins.push('transform-remove-console-plugin');
}
export default defineConfig({
  routes: routes,
  dva: {
    immer: true,
    hmr: false,
  },
  layout: {
    name: '八维创作平台',
    logo: 'https://gss3.bdstatic.com/84oSdTum2Q5BphGlnYG/timg?wapp&quality=80&size=b150_150&subsize=20480&cut_x=0&cut_w=0&cut_y=0&cut_h=0&sec=1369815402&srctrace&di=31ae4c2cac611321b2816410a594ec94&wh_rate=null&src=http%3A%2F%2Fimgsrc.baidu.com%2Fforum%2Fpic%2Fitem%2F9f5b6ac2d5628535ddc0f85898ef76c6a7ef63b5.jpg',
  },
  antd: {
    dark: false,
  },
  nodeModulesTransform: {
    type: 'none',
    exclude: [],
  },
  extraBabelPlugins,
  externals: {
    Echart: 'window.echarts',
  },
  scripts: ['https://cdn.jsdelivr.net/npm/echarts@5.2.0/dist/echarts.min.js'],
  chainWebpack: (config) => {
    // 配置monaco-editor插件只加载markdown语法样式
    config.merge({
      plugins: [
        new MonacoWebpackPlugin({
          languages: ['markdown'],
        }),
      ],
    });
  },
});
