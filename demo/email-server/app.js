const Koa = require('koa');

const Router = require('koa-router')();
const nodemailer = require('nodemailer');
const fs = require('fs');
const app = new Koa();
const dataJson = require('path').join(__dirname, './data/data.json');
app.use(require('koa-bodyparser')());
const sendEmail = async (email, code) => {
  let transporter = nodemailer.createTransport({
    host: 'smtp.163.com',
    port: 25,
    secure: false, // true for 465, false for other ports
    auth: {
      user: 'yihang_i@163.com', // generated ethereal user
      pass: 'YLPICGHWKESASFLH', // generated ethereal password
    },
  });

  // send mail with defined transport object
  let info = await transporter.sendMail({
    from: '注册八维创作平台 <yihang_i@163.com>', // sender address
    to: email, // list of receivers
    subject: '注册八维创作平台', // Subject line
    text: '您的验证码是', // plain text body
    html: `<div><h2>欢迎注册八维创作平台</h2><p>您的验证码为:${code}</p></div>`, // html body
  });

  console.log('Message sent: %s', info.messageId);
  // Message sent: <b658f8ca-6296-ccf4-8306-87d57a0b4321@example.com>

  // Preview only available when sending through an Ethereal account
  console.log('Preview URL: %s', nodemailer.getTestMessageUrl(info));
  // Preview URL: https://ethereal.email/message/WaQKMgKddxQDoou...
  return info;
};
Router.post('/api/getEmailcode', async (ctx) => {
  const { email } = ctx.request.body;
  if (/^\w+@+\w+\.\w+$/.test(email)) {
    // 发邮件
    try {
      const code = Math.random().toString().slice(2, 6);
      await sendEmail(email, code);
      fs.writeFileSync(
        dataJson,
        JSON.stringify({
          code,
          email,
          startTime: Date.now(),
        }),
      );
      ctx.body = {
        code: 'SUCCESS',
        msg: '邮件发送成功',
        data: null,
      };
    } catch (error) {
      ctx.body = {
        code: 'FAIL',
        msg: error.message,
        data: error,
      };
    }
  } else {
    ctx.body = {
      code: 'FAIL',
      msg: '您的邮箱格式不正确',
      data: null,
    };
  }
});

Router.post('/api/registry', async (ctx) => {
  const { email, code } = ctx.request.body;
  try {
    const data = JSON.parse(fs.readFileSync(dataJson, 'utf8'));
    if (email && code) {
      if (
        email === data.email &&
        code === data.code &&
        Date.now() - data.startTime < 1000 * 60 * 5
      ) {
        ctx.body = {
          code: 'SUCCESS',
          msg: '注册成功',
        };
      } else {
        ctx.body = {
          code: 'FAIL',
          msg: '注册失败',
        };
      }
    }
  } catch (error) {
    console.log(error);
  }
});

app.use(Router.routes(), Router.allowedMethods());

app.listen('3000', () => {
  console.log('port is 3000');
});
