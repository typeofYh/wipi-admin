class Redux {
  constructor(reducer, initState) {
    if (typeof reducer !== 'function') {
      throw new Error('reducer 必须是一个函数');
    }
    this._reducer = reducer;
    this._initState = initState;
    this._state = initState;
    this._subscribeCk = [];
    this.dispatch = this.dispatch.bind(this);
    this._init();
  }
  _init() {
    this._state = this._reducer(this._initState, {
      type: '@@redux/INITc.1.s.p.e.v',
    });
  }
  dispatch(action) {
    if (typeof action === 'object' && action.type) {
      this._state = this._reducer(this._state, action);
      this.emit();
      return;
    }
    throw new Error('action 必须要有type');
  }
  emit() {
    this._subscribeCk.forEach((ck) => {
      ck();
    });
  }
  subscribe(callback) {
    this._subscribeCk.push(callback);
  }
  getState() {
    return this._state;
  }
}

const createStore = (reducer, initState) => {
  return new Redux(reducer, initState);
};
