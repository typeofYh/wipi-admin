import { InitialState } from "./app"
export default function(initialState:InitialState){
  console.log(initialState);
  const { role } = initialState;
  return {
    canUser: role === 'admin'
  }
}