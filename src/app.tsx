import { RequestConfig, getDvaApp } from 'umi';
import { createLogger } from 'redux-logger';
import { message } from 'antd';
import { ProBreadcrumb } from '@ant-design/pro-layout';
import './style/index.less';
export const layout = () => {
  return {
    rightContentRender: () => <div>rightContentRender</div>,
    footerRender: () => <div>footer</div>,
    headerContentRender: () => {
      return (
        <div className={'headerContent'}>
          <ProBreadcrumb />
        </div>
      );
    },
    breadcrumbRender: (route: any[]) =>
      [
        {
          path: '/',
          breadcrumbName: '工作台',
        },
      ].concat(
        route.length === 1 && route[0].path === '/'
          ? [{ breadcrumbName: '' }]
          : route,
      ),
    onPageChange: () => {
      console.log('onPageChange');
    },
  };
};

export interface InitialState {
  role: string;
  [propName: string]: any;
}

export const getInitialState = async () => {
  try {
    const userInfo = JSON.parse(localStorage.getItem('userInfo') as string);
    return (
      userInfo || {
        role: '',
      }
    );
  } catch (error) {
    return {
      role: '',
    };
  }
};

export const dva = {
  config: {
    onAction: createLogger(),
    onError(e: Error) {
      message.error(e.message, 3);
    },
  },
};

// window.Dva = getDvaApp();
//dvajs  简化redux操作流程 继承redux-saga
