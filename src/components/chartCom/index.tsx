import { useRef, useEffect, forwardRef, useImperativeHandle } from 'react';
import styles from './style.less';
import * as echatrs from 'Echart';
interface Props {
  width: number;
  height: number;
  option: {
    [propName: string]: any;
  };
  ref: any;
}
const ChartCom = ({ width = 150, height = 100, option }: Props, ref: any) => {
  const container = useRef(null);
  const echartsInstance = useRef(null);
  useEffect(() => {
    echartsInstance.current = echatrs.init(container.current);
  }, []);

  useEffect(() => {
    echartsInstance.current.setOption(option);
  }, [option]);

  useImperativeHandle(ref, () => ({
    refresh: (option: any) => {
      echartsInstance.current.setOption(option);
    },
  }));

  return (
    <div
      className={styles.chartContainer}
      ref={container}
      style={{ width, height }}
    ></div>
  );
};

export default forwardRef(ChartCom);
