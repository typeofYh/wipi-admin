import { ConnectProps, connect } from 'umi';
import { useEffect } from 'react';
import { Table } from 'antd';
import { ActicleModelState } from '@/models/article';
enum StatusMap {
  publish = '已发布',
  draft = '草稿',
}
const statusMap = {
  publish: '已发布',
  draft: '草稿',
};
interface Props extends ConnectProps {
  article: ActicleModelState;
}
function IndexPage(props: Props) {
  useEffect(() => {
    props.dispatch!({
      type: 'article/getList',
    });
  }, []);
  const columns = [
    {
      title: '标题',
      dataIndex: 'title',
    },
    {
      title: '状态',
      dataIndex: 'status',
      render: (text: string) => {
        return (
          <span className={'status'}>{statusMap[text] || '暂无状态'}</span>
        );
      },
    },
  ];
  return (
    <div>
      <h1>Page 所有文章</h1>
      <Table columns={columns} dataSource={props.article.list} />
    </div>
  );
}

export default connect((data) => data)(IndexPage);
