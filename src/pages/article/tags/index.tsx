import MonacoEditor from 'react-monaco-editor';
import hljs from 'highlight.js';
import MarkdownIt from 'markdown-it';
import { useEffect, useRef, useState } from 'react';
export default function IndexPage() {
  const contariner = useRef(null);
  const [html, setHtml] = useState('');
  useEffect(() => {
    contariner.current = new MarkdownIt({
      html: false,
      xhtmlOut: false,
      breaks: false,
      langPrefix: 'language-',
      linkify: true,
      typographer: false,
      quotes: '“”‘’',
      highlight: (str: string, lang: string) => {
        // 此处判断是否有添加代码语言
        if (lang && hljs.getLanguage(lang)) {
          try {
            // 得到经过highlight.js之后的html代码
            const preCode = hljs.highlight(lang, str, true).value;
            // 通过换行进行分割
            const lines = preCode.split(/\n/).slice(0, -1);
            // 添加自定义行号
            let html = lines
              .map((item, index) => {
                return (
                  '<li><span class="line-num data-line="' +
                  (index + 1) +
                  '"></span>' +
                  item +
                  '</li>'
                );
              })
              .join('');
            html = '<ol>' + html + '</ol>';
            // 添加代码语言//
            if (lines.length) {
              html += '<b class="name">' + lang + '</b>';
            }
            return '<pre class="hljs"><code>' + html + '</code></pre>';
          } catch (_) {
            console.log(_);
          }
        }
        // 为添加代码语言，此处与上面同理
        const preCode: any = contariner.current.utils.escapeHtml(str);
        const lines = preCode.split(/\n/).slice(0, -1);
        let html = lines
          .map((item: any, index: number) => {
            return (
              '<li><span class="line-num data-line="' +
              (index + 1) +
              '"></span>' +
              item +
              '</li>'
            );
          })
          .join('');
        html = '<ol>' + html + '</ol>';
        return '<pre class="hljs"><code>' + html + '</code></pre>';
      },
    });
  }, []);
  const handleChange = (value: string) => {
    const res = contariner.current.render(value);
    setHtml(res);
  };
  return (
    <div>
      <h1>Page 文章标签</h1>
      <div>
        <MonacoEditor
          width="800"
          height="600"
          language="markdown"
          onChange={handleChange}
        />
        <div
          className="markdown"
          dangerouslySetInnerHTML={{ __html: html }}
        ></div>
      </div>
    </div>
  );
}

// monaco-editor 支持markdwon
// monaco-editor-webpack-plugin 扩展webpack配置仅支持markdown语法

// react-monaco-editor 对monaco-editor进行封装，输出组件直接调用组件

// 以上三个包针对编辑器

// 预览功能
// markdown-it 支持语言高亮
// highlight.js 配置代码块语言高亮  ```js  ```
