export default {
  title: {
    text: '每周用户访问指标',
  },
  legend: {
    data: ['评论数', '访问量'],
  },
  xAxis: [
    {
      type: 'category',
      data: ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'],
      axisPointer: {
        type: 'shadow',
      },
    },
  ],
  yAxis: [
    {
      type: 'value',
      name: '评论数',
      min: 0,
      max: 250,
      interval: 50,
    },
  ],
  series: [
    {
      name: '评论数',
      type: 'bar',
      data: [],
    },
    {
      name: '访问量',
      type: 'line',
      data: [],
    },
  ],
};
