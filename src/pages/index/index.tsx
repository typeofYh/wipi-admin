import styles from './index.less';
import ChartCom from '@/components/chartCom';
import { useRequest } from 'umi';
import { getData } from '@/services/user';
import { useEffect, useState, useRef } from 'react';
import option from './chartConfig';
export default function IndexPage() {
  const { run } = useRequest(getData, { manual: true });
  const [optionData, setOption] = useState(option);
  const charCom = useRef(null);
  const setChartOption = (data: any) => {
    option.series[1].data = data.VisitsData.map(({ value }: any) => value);
    option.series[0].data = data.commentData.map(({ value }: any) => value);
    setOption({ ...option });
  };
  useEffect(() => {
    run().then((data) => {
      setChartOption(data);
    });
  }, []);
  return (
    <div className={styles.container}>
      <h1 className={styles.title}>Page index</h1>
      <ChartCom width={500} height={400} option={optionData} ref={charCom} />
      <button
        onClick={() => {
          console.log(charCom.current);
        }}
      >
        refresh
      </button>
    </div>
  );
}
