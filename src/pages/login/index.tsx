import styles from './index.less';
import React from 'react';
import { login } from "@/services/user";
import { Button } from "antd";
import { useLocalStorageState } from "ahooks";
import { useHistory, useRequest, useModel } from 'umi';
export default function IndexPage() {
  const history = useHistory();
  const {run, loading} = useRequest(login,{manual:true});
  const { setInitialState } = useModel('@@initialState');
  const [,setUser] = useLocalStorageState('userInfo',{});
  const handleLogin = async () => {
    // TODO: 获取文本框值
    const data = await run({
      name:"yihang", 
      password: "yihang"
    })
    setInitialState && setInitialState(data);
    setUser(JSON.stringify(data));
    history.push('/');
  }
  return (
    <div>
      <h1 className={styles.title}>login index</h1>
      <Button onClick={handleLogin} loading={loading}>登录</Button>
    </div>
  );
}
