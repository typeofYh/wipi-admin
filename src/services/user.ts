import { request } from 'umi';

export interface loginData {
  name: string;
  password: string;
}

export interface PageParams {
  page: number;
  pageSize: number;
}

export const login = (data: loginData) =>
  request('/login', {
    method: 'post',
    data,
    skipErrorHandler: true,
  });

export const getArticleList = (params: PageParams) =>
  request('/getArticleList', {
    params,
  });

export const getData = () => request('/userData');
